# Tutorial_UART_to_DMA

## Project overview
This project aims to serve as a tutorial for memory to memory transfer using DMA. The technique for DMA transfer is with interrupt.

## Prerequisites
- The board used in this tutorial is a __NUCLEO-F746ZG__
- Fair knowledge in using STM32CubeIDE and STM32CubeMX applications. 
- Previous reading of this [WIKI.](https://wiki.st.com/stm32mcu/wiki/Getting_started_with_DMA)

## Brief knowledge items
- DMA means DIRECT MEMORY ACCESS and it is used in MCU applications for various transfers between 
  peripherals and memory or memory to memory, all without CPU intervention. It is a very useful 
  controller for unattended transfers.
  
## Application Workflow
- DMA intialization is generated in `main.c`.
- `HAL_DMA_Start_IT`: Start DMA buffer transfer
- `DMA1_Channel1_IRQHandler` is generated in `stm32f4xx_it.c`: it indicates whether the DMA process is half/complete or an error was detected.
- `HAL_DMA_IRQHandler` is defined in `stm32f4xx_hal_dma.c`: Process interrupt information.
- `DMA_XferCpltCallback`: Data correctly transferred __complete callback function.__
- `DMA_XferErrorCallback`: __Error was detected Error callback function.__

- For more information about DMA transfer workflows, read the 
[**reference manual**](https://www.st.com/resource/en/reference_manual/rm0385-stm32f75xxx-and-stm32f74xxx-advanced-armbased-32bit-mcus-stmicroelectronics.pdf), 
[**HAL user manual**](https://www.st.com/resource/en/user_manual/um1905-description-of-stm32f7-hal-and-lowlayer-drivers-stmicroelectronics.pdf) and 
[**application note AN4031**](https://www.st.com/resource/en/application_note/dm00046011-using-the-stm32f2-stm32f4-and-stm32f7-series-dma-controller-stmicroelectronics.pdf) 


![DMA with interrupts workflow!](./Docs/TUT_res/DMA_workflow_with_IT.png "DMA with interrupts workflow")

## Building steps
### Step 1 - Create a project in STM32CubeIDE
- Create new STM32 Project;
- Select Board Selector, choose NUCLEO-F746ZG board;
- Name the project as you wish;
- Initialize board settings in default mode.

### Step 2 - Configure DMA channels in STM32CubeMx
- Choose default clock configuration or customize as you wish.
- Add DMA channel from __Pinout & Configuration__ tab and configure it as follows:
	- MEMTOMEM DMA request : DMA1 Channel 1
	- ___Normal mode__
	- __Increment__ source and destination addresses
	- __Byte__ data width
- Enable `Force DMA channels Interrupts` option from __System Core -> NVIC__ in **NVIC** tab
- Enable `DMA1 channel global interrupt` from __System Core -> NVIC__ in **NVIC** tab

### Step 3 - Generate code and modify main.c file
- We create the callback function for complete transfer and two buffers as source and destination buffers as follows:
	
	In `main.c` file we insert following code between the tags `/* USER CODE BEGIN 0 */` and `/* USER CODE END 0 */`
	
	```
	/* USER CODE BEGIN 0 */
	void XferCpltCallback(DMA_HandleTypeDef *hdma);
	uint8_t Buffer_Src[]={0,1,2,3,4,5,6,7,8,9};
	uint8_t Buffer_Dest[10];
	/* USER CODE END 0 */
	``` 
	
- Before we start the DMA with interrupt, we need to set the callback into DMA structure.
  After that, it is possible to use `HAL_DMA_Start_IT` to begin the DMA transfer.
  
  	In `main.c` file we insert following code between the tags `/* USER CODE BEGIN 2 */` and `/* USER CODE END 2 */`
  
 	``` 
	/* USER CODE BEGIN 2 */
	hdma_memtomem_dma2_stream0.XferCpltCallback=&XferCpltCallback;
	HAL_DMA_Start_IT(&hdma_memtomem_dma2_stream0,(uint32_t)Buffer_Src,(uint32_t)Buffer_Dest,10);
	/* USER CODE END 2 */
	```
 	
	
- Finally, we define the `XferCpltCallback` function between the tags  `/* USER CODE BEGIN 4 */` and `/* USER CODE END 4 */`
	
	``` 
	/* USER CODE BEGIN 4 */
	void XferCpltCallback(DMA_HandleTypeDef *hdma)
	{	
		__NOP(); //Line reached only if transfer was successful. Toggle a breakpoint here
	}
	/* USER CODE END 4 */
	```	
	
 
### Step 4 - Save, Compile, Flash and Run 

<br>
<br>
<br>